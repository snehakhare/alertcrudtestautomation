package com.alert.crud.uitests;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.alert.crud.pages.AddComputerData;
import com.alert.crud.pages.ComputerLisPage;
import com.alert.crud.pages.EditDeleteComputerData;
import com.alert.crud.tests.CommonTestBase;

public class ComputerCRUDTest extends CommonTestBase {

	@Test
	public void crudTest() {

		driver.get("http://computer-database.gatling.io/computers");

		ComputerLisPage listPage = new ComputerLisPage(driver);

		Assert.assertEquals(listPage.isPageOpened(), true,
				"Failed to load URL:http://computer-database.gatling.io/computers");

		listPage.clickForAddComputer();

		AddComputerData compData = new AddComputerData(driver);
		compData.computerNameData("sk6565");
		compData.discontinuedDateData("2023-01-01");
		compData.intoducedDateData("2020-01-01");
		compData.enterData("IBM");
		compData.clickComputerData();

		Assert.assertEquals(listPage.isSucessMessage(), true);

		listPage.enterData("sk6565");

		listPage.searchSubmmit();

		listPage.clickComputerName();

		EditDeleteComputerData editData = new EditDeleteComputerData(driver);

		editData.updateIntroduced("2022-02-01");

		editData.UpdateSubmmit();

		Assert.assertEquals(listPage.isSucessMessage(), true);

		listPage.enterData("sk6565");

		listPage.searchSubmmit();

		listPage.clickComputerName();
		
		editData.clickForDeleteComputer();
		
		Assert.assertEquals(listPage.isSucessMessage(), true);

	}
}
