package com.alert.crud.uitests;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.alert.crud.pages.AlertPage;
import com.alert.crud.tests.CommonTestBase;

public class AlertsTest extends CommonTestBase {

	@Test
	public void manageJSAlert() {

		driver.get("https://the-internet.herokuapp.com/javascript_alerts");

		AlertPage alertPage = new AlertPage(driver);

		Assert.assertEquals(alertPage.isPageOpened(), true,
				"Failed to load URL:https://the-internet.herokuapp.com/javascript_alerts");

		alertPage.clickJSAlertBtn();

		Assert.assertEquals(alertPage.getAlertText().contains("I am a JS Alert"), true);

		alertPage.clickAlertOk();

		Assert.assertEquals(alertPage.resultData().contains("You successfuly clicked an alert"), true);

		alertPage.clickJSConfirmAlertBtn();

		Assert.assertEquals(alertPage.getAlertText().contains("I am a JS Confirm"), true);

		alertPage.clickAlertOk();

		Assert.assertEquals(alertPage.resultData().contains("You clicked: Ok"), true);

		alertPage.clickJSConfirmAlertBtn();

		Assert.assertEquals(alertPage.getAlertText().contains("I am a JS Confirm"), true);

		alertPage.clickAlertCancel();

		Assert.assertEquals(alertPage.resultData().contains("You clicked: Cancel"), true);
		
		alertPage.clickJSPromtAlertBtn();
		
		Assert.assertEquals(alertPage.getAlertText().contains("I am a JS prompt"), true);
		
		alertPage.sendTextToAlertAndAccept("Alert");
		
		
		Assert.assertEquals(alertPage.resultData().contains("You entered: Alert"), true);

		driver.quit();

	}

	
	  
	 

}
