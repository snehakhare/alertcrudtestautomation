package com.alert.crud.browsers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;

public class IEBrowser extends Browser {
	
	private WebDriver driver;
	
	public IEBrowser(){
		driver = new InternetExplorerDriver();
	}

	@Override
	public WebDriver getWebDriver() {
		return driver;
	}

}
