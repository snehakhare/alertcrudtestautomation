package com.alert.crud.browsers;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;

public abstract class Browser {
	
	private static final Logger LOGGER = Logger.getLogger(Browser.class);
	public static final String CAPTURE_PATH = System.getProperty("capture-path" , "targe/failure");
	
	public void openURL(String url){
		
		getWebDriver().get(url);
	}
	
	abstract WebDriver getWebDriver();

}
