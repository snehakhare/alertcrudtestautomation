package com.alert.crud.browsers;

public enum BrowserType {
	
	CHROME, FIREFOX, HTML_UNIT, IE

}
