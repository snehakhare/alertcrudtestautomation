package com.alert.crud.browsers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class ChromeBrowser extends Browser {

	private WebDriver driver;

	public ChromeBrowser() {
		
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir") + "/drivers/chromedriver.exe");
		driver = new ChromeDriver();
	}

	@Override
	public WebDriver getWebDriver() {

		return driver;
	}

}
