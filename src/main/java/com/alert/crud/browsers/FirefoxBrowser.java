package com.alert.crud.browsers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class FirefoxBrowser extends Browser {

	private WebDriver driver;

	public FirefoxBrowser() {
		driver = new FirefoxDriver();
	}

	@Override
	public WebDriver getWebDriver() {
		return driver;
	}

}
