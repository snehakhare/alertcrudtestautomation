package com.alert.crud.tests;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import com.alert.crud.browsers.BrowserType;
import com.alert.crud.browsers.ChromeBrowser;
import com.alert.crud.browsers.FirefoxBrowser;
import com.alert.crud.browsers.IEBrowser;

public class CommonTestBase {

	private static final Logger LOGGER = Logger.getLogger(CommonTestBase.class);

	protected WebDriver driver;
	protected WebDriverWait wait;

	@BeforeClass
	public void setuBrowser() {

		BrowserType type = BrowserType.valueOf(System.getProperty("browser-type", getDefualtBrowserType().name()));
		this.driver = createBrowser(type);
		this.wait = new WebDriverWait(this.driver, 20);
	}
	
	@AfterClass
	public void tearDown() {
		driver.quit();
	}

	private BrowserType getDefualtBrowserType() {
		return BrowserType.CHROME;
	}

	private WebDriver createBrowser(BrowserType type) {
		LOGGER.info("Creating Browser of type:" + type);

		switch (type) {
		case CHROME:
			return new ChromeBrowser().getWebDriver();
		case IE:
			return new IEBrowser().getWebDriver();
		case FIREFOX:
			return new FirefoxBrowser().getWebDriver();
		default:
			throw new IllegalArgumentException(type.name());

		}

	}
	
	public WebDriver getDriver(){
		return driver;
	}

}
