package com.alert.crud.pages;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;


public class EditDeleteComputerData extends BasePage {
	
	 @FindBy(css = "#main > h1")
	   private WebElement pageHeading1;
	
	   @FindBy(css = "#main > form.topRight > input")
	   private WebElement clickForDeleteComputer;
	   
	   @FindBy(css = "#introduced")
	   private WebElement Updateintroduced;
	   
	   @FindBy(css = "#main > form:nth-child(2) > div > input")
	   private WebElement updateSubmmit;	   
	   
	 //Constructor
	   public EditDeleteComputerData (WebDriver driver){
	       init(driver);
	   }
	   
	   public void clickForDeleteComputer() {
		   this.clickForDeleteComputer.click();
	   }
	   
	   public void updateIntroduced(String searchText){
		   wait.until(ExpectedConditions.visibilityOf(this.Updateintroduced));
		   this.Updateintroduced.clear();
		   this.Updateintroduced.sendKeys(searchText);	     
	   }
	   
	   
	   public void UpdateSubmmit() {
		   this.updateSubmmit.click();
	   }

}
 	