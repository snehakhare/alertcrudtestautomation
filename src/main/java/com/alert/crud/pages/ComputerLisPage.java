package com.alert.crud.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class ComputerLisPage extends BasePage {
	
	 @FindBy(css = "body > header > h1 > a")
	   private WebElement pageHeading1;
	
	   @FindBy(css = "#add")
	   private WebElement clickForAddComputer;
	   
	   @FindBy(css = "#searchbox")
	   private WebElement textForSearch;
	   
	   @FindBy(css = "#searchsubmit")
	   private WebElement searchSubmmit;
	   
	   @FindBy(css = "td > a[href^=\"/computers/\"]")
	   private WebElement firstComputerName;
	   
	   @FindBy(css = "#main > table > tbody > tr > td:nth-child(2)")
	   private WebElement firstIntroduced;
	   
	   @FindBy(css = "#main > table > tbody > tr > td:nth-child(3)")
	   private WebElement firstDiscontinued;
	   
	   @FindBy(css = "#main > table > tbody > tr > td:nth-child(4)")
	   private WebElement firstCompany;
	   
	   @FindBy(css = "#main > div.alert-message.warning")
	   private WebElement sucessMessage;
	   
	   //Constructor
	   public ComputerLisPage (WebDriver driver){
	       init(driver);
	   }
	   
	   public boolean isPageOpened(){
		   wait.until(ExpectedConditions.visibilityOf(this.pageHeading1));
	       return this.pageHeading1.getText().contains("Play");
	   }
	   
	   public boolean isSucessMessage(){
		   wait.until(ExpectedConditions.visibilityOf(this.sucessMessage));
		   //System.out.println(">>>>" + sucessMessage.getText());
	       return sucessMessage.getText().contains("Done");
	   }

	   public void clickForAddComputer() {
		   this.clickForAddComputer.click();
	   }
	   
	   public void enterData(String searchText){
		   wait.until(ExpectedConditions.visibilityOf(this.textForSearch));
		   this.textForSearch.sendKeys(searchText);	     
	   }
	   
	   public void searchSubmmit() {
		   this.searchSubmmit.click();
	   }
	   
	   public String getFirstComputerName() {
		   return this.firstComputerName.getText();
	   }
	   
	   public String getFirstIntroduced() {
		   return this.firstIntroduced.getText();
	   }
	   
	   public String getFirstDiscontinued() {
		   return this.firstDiscontinued.getText();
	   }
	   
	   public String getFirstCompany() {
		   return this.firstCompany.getText();
	   }
	   
	   public void clickComputerName() {
		   	wait.until(ExpectedConditions.visibilityOf(this.firstComputerName));
		    this.firstComputerName.click();
	   }
	   
}


