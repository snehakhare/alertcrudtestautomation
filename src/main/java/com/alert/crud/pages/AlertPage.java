package com.alert.crud.pages;

import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class AlertPage extends BasePage {
	
	   @FindBy(css = "#content > div > h3")
	   private WebElement pageHeading;
	
	   @FindBy(css = "button[onclick=\"jsAlert()\"]")
	   private WebElement clickForJSAlertBtn;
	   
	   @FindBy(css = "button[onclick=\"jsConfirm()\"]")
	   private WebElement clickForJSConfirmAlertBtn;
	   
	   @FindBy(css = "button[onclick=\"jsPrompt()\"]")
	   private WebElement clickForJSPromtAlertBtn;
	   
	   @FindBy (css = "#result")
	   private WebElement resultData;

	   
	   //Constructor
	   public AlertPage (WebDriver driver){
	       init(driver);
	   }

	   //We will use this boolean for assertion. To check if page is opened
	   public boolean isPageOpened(){
		   wait.until(ExpectedConditions.visibilityOf(pageHeading));
	       return pageHeading.getText().toString().contains("JavaScript Alerts");
	   }
	   
	   public String resultData(){
	       return resultData.getText();
	   }
	   
	   public void clickJSAlertBtn() {
		   wait.until(ExpectedConditions.visibilityOf(this.clickForJSAlertBtn));
		   this.clickForJSAlertBtn.click();
	   }
	   
	   public void clickJSConfirmAlertBtn() {
		   wait.until(ExpectedConditions.visibilityOf(this.clickForJSConfirmAlertBtn));
		   this.clickForJSConfirmAlertBtn.click();
	   }
	   
	   public void clickJSPromtAlertBtn() {
		   wait.until(ExpectedConditions.visibilityOf(this.clickForJSPromtAlertBtn));
		   this.clickForJSPromtAlertBtn.click();
	   }
	   
	   
	   public void clickAlertOk(){
		   Alert alert = driver.switchTo().alert();
		   alert.accept();
	   }
	   

	   public void clickAlertCancel(){
		   Alert alert = driver.switchTo().alert();
		   alert.dismiss();
		   
	   }
	   
	   public String getAlertText(){
		   Alert alert = driver.switchTo().alert();
		   return alert.getText();
		   
	   }
	   
	   public void sendTextToAlertAndAccept(String alertText){
		   Alert alert = driver.switchTo().alert();
		   alert.sendKeys(alertText);
		   alert.accept();		   
	   }

}
