package com.alert.crud.pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;


public abstract class BasePage {
	
	protected WebDriver driver;
	protected WebDriverWait wait;
	
	private static final Logger LOGGER = Logger.getLogger(BasePage.class);
	
	
	public void init(WebDriver driver){
		this.driver = driver;
		this.wait = new WebDriverWait(this.driver, Integer.parseInt(System.getProperty("webdriver-wait", "20")));
		PageFactory.initElements(driver, this);
		System.out.println("Page Object : " + this.getClass());
	}
	
	public void openURL(String url){
		driver.get(url);
	}
	
	public void quiteDriver(){
		driver.quit();
	}
	
	public String getPageTitle(){
		return driver.getTitle();
	}

}
