package com.alert.crud.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

public class AddComputerData  extends BasePage {
	
	@FindBy(css = "#main > h1")
	   private WebElement pageHeading;
	
	   @FindBy(css = "#name")
	   private WebElement computerName;
	   
	   @FindBy(css = "#introduced")
	   private WebElement intoducedDate;
	   
	   @FindBy(css = "#discontinued")
	   private WebElement discontinuedDate;
	   
	   @FindBy (id = "company")
	   private WebElement companyDropDown;
	   
	   @FindBy(css = "#main > form > div > input")
	   private WebElement addComputerData;
	   
	   //Constructor
	   public AddComputerData (WebDriver driver){
	       init(driver);
	   }
	   
	   public void computerNameData(String searchText){
		   this.computerName.sendKeys(searchText);	     
	   }
	   
	   public void intoducedDateData(String searchText){
		   this.intoducedDate.sendKeys(searchText);	     
	   }
	   
	   public void discontinuedDateData(String searchText){
		   this.discontinuedDate.sendKeys(searchText);	     
	   }
	   
	   public void enterData(String searchText){
		   Select compSelect = new Select(this.companyDropDown);
		   compSelect.selectByVisibleText(searchText);
		   
	   }
	   
	   public void clickComputerData() {
		   this.addComputerData.click();
	   }
     
	   
}
