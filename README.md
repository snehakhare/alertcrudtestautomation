# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This repository has the automated browser-based test to get a car/insurance quote.
* v1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Using IDE ###


* Install Eclipse
* Install latest JDK
* Import project from repository
* Running with Firefox Browser: Maven Configuration: clean test
* To Run with other browser: Maven Configuration: -Dbrowesr-type=BROWSERNAME clean test

### Using without IDE ###
* Install latest JDK
* Install maven
* clone project from repository
* Navigate to project folder and run using maven command: 
* For Default Browser: mvn clean test
* For other browser: mvn -Dbrowesr-type=BROWSERNAME clean test

NOTE: This project is build with Selenium version 2.53.1 and Firefox 47.0.2.
If you receive below error then please downgrade your firefox to 47.0.2.

org.openqa.selenium.firefox.NotConnectedException: Unable to connect to host 127.0.0.1 on port 7055 after 45000 ms